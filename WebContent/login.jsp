<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="./css/login.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
    </head>
    <body>
        <div class="login-contents">
			<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
			<div class="login">
            <form action="login" method="post" class="loginname">
                <div class="account"><label for="accountname">アカウント名</label>
                <input name="account" value="${account}" id="account"/></div>

                <div class="password"><label for="passwordname">パスワード </label>
                <input name="password" type="password" id="password"/></div>

                <input type="submit" value="ログイン" class="loginbutt" />
            </form>
            </div>
            <div class="copyright">Copyright(c)Sanuki.Yutaro</div>
        </div>
    </body>
</html>