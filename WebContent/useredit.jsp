<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="./css/edit.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>
           <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="update" method="post" class="edit"><br />
            <div class="useredit">ユーザー編集画面</div>
                <input name="id" value="${User.id}" id="id" type="hidden"/>
                <input name="loginId" value="${loginUser.id}" id="loginId" type="hidden"/>
                <input name="defacc" value="${User.account}" id="defacc" type="hidden"/>
                <div class="name">
                <label for="name" class="name1">名前(10文字以下で入力してください)</label><br />
                <input name="name" value="${EditUser.name}" id="name"/><br />
				</div>
				<div class="account">
                <label for="account" class="account1">アカウント名（半角英数字6文字以上20文字以下で入力してください)</label><br />
                <input name="account" value="${EditUser.account}" /><br />
				</div>
				<div class="password1">
                <label for="password" class="password11">パスワード(記号を含む半角英数字6文字以上20文字以下で入力してください)</label><br />
                <input name="password" type="password" id="password"/> <br />
				</div>
				<div class="password2">
				<label for="password" class="password21">確認用パスワード(上記と同じ)</label><br />
                <input name="passwordcopy" type="password" id="passwordcopy"/> <br />
				</div>
				<c:if test="${loginUser.id != User.id}" var="flg2" />
                <c:if test="${flg2}" >
                	支店名<br/>
                <select name="branchId">
                <c:forEach items="${Branch}" var="branch">
			     <c:if test="${User.branchId == branch.id }" var="flg" />
			     <c:if test="${flg}" >
				<option value="${ branch.id}" selected="selected">${branch.name}</option>
				</c:if>
				<c:if test="${!flg}" >
				<option value="${ branch.id}">${branch.name}</option>
				</c:if>
				</c:forEach>
				</select>
				</c:if>
				<c:if test="${!flg2}" ><input type = "hidden" name="branchId"  value="${User.branchId}"  /></c:if><br />

				<c:if test="${flg2}" >
				部署名<br/>
                <select name="positionId">
                <c:forEach items="${Position}" var="position">
			     <c:if test="${User.positionId == position.id }" var="flg" />
			     <c:if test="${flg}" >
				<option value="${ position.id}" selected="selected">${position.name}</option>
				</c:if>
				<c:if test="${!flg}" >
				<option value="${ position.id}">${position.name}</option>
				</c:if>
				</c:forEach>
				</select>
				</c:if>
				 <c:if test="${!flg2}" ><input type = "hidden" name="positionId"  value="${User.positionId}"  /></c:if><br />
			<input type="submit" value="変更" class="update" /> <br />
                <a href="manager" class="back" >戻る</a>
            </form>
            <c:remove var="EditUser" scope="session" />
            <c:remove var="Branch" scope="session" />
            <c:remove var="Position" scope="session" />
            <div class="copyright"> Copyright(c)Sanuki.Yutaro</div>
</body>
</html>