<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/newmessage.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
<div class="form-area">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

        <form action="newMessage" method="post" class="form">
        <div class="postlim">
        <div class="postname">新規投稿</div>
		件名 （30文字まで）<br />
       <input type="text" name="subject" value="${defsubject}" size="30" maxlength="50"><br />
		カテゴリ （10文字まで）<br />
        <input type="text" name="category" value="${defcategory}" size="30" maxlength="50"><br />
		本文 （1000文字まで）<br />
            <textarea name="text" cols="100" rows="20" class="tweet-box">${deftext}</textarea>
            </div>
            <input type="submit" value="投稿" class="post"><br/>
             <a href="./" class="back">戻る</a>
        </form>

</div>
<c:remove var="defsubject" scope="session" />
<c:remove var="defcategory" scope="session" />
<c:remove var="deftext" scope="session" />

 <div class="copyright">Copyright(c)Sanuki.Yutaro</div>
</body>
</html>