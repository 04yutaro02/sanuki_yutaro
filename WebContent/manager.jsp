<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/manager.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>


<script type="text/javascript">

function delcheck(name){
	if(window.confirm( name + 'を停止しますか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

function revcheck(name){
	if(window.confirm( name + 'を復活しますか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}
</script>

</head>
<body>
           <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>


<div class="main">

<div class="title">ユーザー管理<a href="signup" class="signup">新規登録</a></div>
    <table border="5" class="table">
    <tr>
    <th>ログインID</th>
    <th>名前</th>
    <th>支店名</th>
    <th>部署名</th>
    <th>編集</th>
    <th>ユーザー状況</th>
    </tr>

        <c:forEach items="${somename}" var="somename">

                <div class="user">
                <tr>

                    <td>${somename.account}</td>
                    <td>${somename.name}</td>
                    <td>${somename.branchName}</td>
                    <td>${somename.positionName}</td>

                     <form action="useredit" method="get">
                     <input type = "hidden" name="id"  value="${somename.id}"  />
                     <th><input type = "submit" value ="編集" class="edit"></th>
                      </form>
                      <c:if test="${loginUser.id != somename.id}" var="flg1" />
                      <c:if test="${flg1}" >
                      <c:if test="${somename.is_deleted == '0'}" var="flg" />
                      <c:if test="${flg}" >
                     <form action="userdelete" method="post" onSubmit="return delcheck('${somename.name}')">
                     <input type = "hidden" name="id"  value="${somename.id}"  />
                     <input type = "hidden" name="is_deleted"  value="${somename.is_deleted}"  />
                     <th><input type = "submit" value ="アカウント停止" class="delete"></th>
                      </form>
                      </c:if>
                      <c:if test="${!flg}" >
                     <form action="userdelete" method="post" onSubmit="return revcheck('${somename.name}')">
                     <input type = "hidden" name="id"  value="${somename.id}"  />
                     <input type = "hidden" name="is_deleted"  value="${somename.is_deleted}"  />
                     <th><input type = "submit" value ="アカウント復活" class="revive"></th>
                      </form>
                      </c:if>
                      </c:if>
                       <c:if test="${!flg1}" >
                      	<th>ログイン中</th>
                      </c:if>
			</tr>


                <div class="text"><c:out value="${message.text}" /></div>
            </div>
    </c:forEach>
    </table>
    	<div class="menu">
<br/>
        <a href="./" class="back">戻る</a>
    </div>
                  </div>
            <div class="copyright"> Copyright(c)Sanuki.Yutaro</div>
</body>
</html>