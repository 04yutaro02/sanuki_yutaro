<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/signup.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>

<script type = "text/javascript">
function functionName()
    {
        var select1 = document.forms.formName.branchId; //変数select1を宣言
        var select2 = document.forms.formName.positionId; //変数select2を宣言

       select2.options.length = 0; // 選択肢の数がそれぞれに異なる場合、これが重要

        if (select1.options[select1.selectedIndex].value == "1")
            {
                select2.options[0] = new Option("人事総務部" , "1");
                select2.options[1] = new Option("社員" , "3");
                select2.options[2] = new Option("情報管理担当者" , "4");
            }

        else if (select1.options[select1.selectedIndex].value == "2")
            {
                select2.options[0] = new Option("支店長" , "2");
                select2.options[1] = new Option("社員", "3");
            }

        else if (select1.options[select1.selectedIndex].value == "3")
        {
            select2.options[0] = new Option("支店長" , "2");
            select2.options[1] = new Option("社員", "3");
        }


    }

</script>
</head>
<body onLoad ="functionName()">
<div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form name="formName" action="signup" method="post" class="form">
            	<div class="title">ユーザー新規登録</div>
                <br /> <label for="name">名前(10文字以下で入力してください)</label><br /> <input name="name" value="${errorName}" id="name" />
                <br /><label for="account">アカウント名（半角英数字6文字以上20文字以下で入力してください)</label> <br /><input name="account"  value="${errorAccount}"  id="account" />
                <br /> <label for="password">パスワード(記号を含む半角英数字6文字以上20文字以下で入力してください)</label> <br /><input name="password" type="password" id="password" />
                 <br /> <label for="password">パスワード(確認用、上記と同じ)</label> <br /><input name="passwordcopy" type="password" id="passwordcopy" />

				<br />
                	支店名<br />
                <select name="branchId" onChange="functionName()">
				<option value="1">本店</option>
				<option value="2">大阪支店</option>
				<option value="3">名古屋支店</option>
				</select>

				<br />部署名<br />
				<select name = "positionId">
				</select>

				<c:remove var="errorName" scope="session" />
				<c:remove var="errorAccount" scope="session" />


                 <br /><input type="submit" value="登録" class="signup"/> <br /> <a href="manager" class="back">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Sanuki.Yutaro</div>
        </div>

</body>
</html>