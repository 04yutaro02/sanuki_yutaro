<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>トップ</title>

<script type="text/javascript">

function textdel(){
	if(window.confirm('投稿を削除しますか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}

function comdel(){
	if(window.confirm('コメントを削除しますか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}
}


</script>

</head>
<body>

	<c:if test="${ not empty errorMessages }">
    	<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>
	<div class="main-contents">
		<div class="header">トップ</div>
 		<div class ="center">
		<div class="menu">
        <form action="./" method="get" class="searchform">
        	<div class="searchdate">
			投稿日時<br />
			<input type = "date" value='${Start}'  name ="start" class="startdate">から
        	<input type = "date" value='${End}' name ="end" class="enddate">まで
        	</div>
        	<div class="searchcategory">
            <label for="word">カテゴリ</label><br />
            <input name="word" id="word" value="${Word}"/>
            <input type = "submit" value ="検索" class="search"><br />
            <a href="./" class="reset">リセット</a>
            </div>
		</form>
    	<c:if test="${ empty loginUser }">
        	<a href="login">ログイン</a><br />
    	</c:if>
    	<c:if test="${ not empty loginUser }">
    	<div class="menutext">
        	<c:if test="${loginUser.positionId == 1 and loginUser.branchId == 1}">
        		<a href="manager" class="manager">ユーザー管理</a><br />
    		</c:if>

       	<a href="newMessage" class="newmessage">新規投稿</a><br />
       	        	<a href="logout" class="logout">ログアウト</a><br />
        	</div>
    	</c:if>
    	<c:if test="${ not empty loginUser }">
    		<div class="profile">
				<div class="name">Name:<c:out value="${loginUser.name}" /></div>
			</div><br />
		</c:if>
		</div>

			<div class="messages">
				<c:forEach items="${messages}" var="message">
				<div class="messa">
	    				<span class="subject"><c:out value="${message.subject}" /></span>
	    				<span class="category">カテゴリ：<c:out value="${message.category}" /></span>
	            		<div class="message">
							<c:forEach var="s" items="${fn:split(message.text, '
')}">
	    					<div>${s}</div>
							</c:forEach>

							<div class="account-name">
								<c:out value="${message.name}" /><br />
							</div>
	                			<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
	            		</div><!--messageの-->
	            		<c:if test="${loginUser.id == message.userId }" var="flg" />
	            		<c:if test="${flg}" >
							<form action="messagedelete" method="post" onSubmit="return textdel()">
								<input type = "hidden" name="id"  value="${message.id}"  />
								<input type = "submit" value ="投稿削除" class="messagedel">
							</form>
				 		</c:if>
				 </div>


				<c:forEach items="${comments}" var="comment">

					<c:if test="${message.id == comment.message_id }" var="flg" />
						<c:if test="${flg}" >
						<div class="comments">
							<div class="comment"><c:forEach var="s" items="${fn:split(comment.text, '
')}">
    						<div>${s}</div>

							</c:forEach>
							</div>
			     			<span class="commentName"><c:out value="${comment.user_name}" /></span>
			     			<span class="commentDate"><fmt:formatDate value="${comment.createdDate}"  pattern="yyyy/MM/dd HH:mm:ss"/></span>
						<c:if test="${!flg}" ></c:if>
						<c:if test="${loginUser.id == comment.user_id }" var="flg" />
							<c:if test="${flg}" >
								<form action="commentDelete" method="post" onSubmit="return comdel()">
									<input type = "hidden" name="commentId"  value="${comment.id}"  />
									<input type = "submit" value ="コメント削除" class="commentdel">
								</form>
			 				</c:if>
			 				</div>
						</c:if>

				</c:forEach>

			<!--commentsの-->
			<div class="newcomment">
        <form action="newComment" method="post">
			<div class="commentlim">コメント （500文字まで）<br /></div>
            <textarea name="commentText" cols="124" rows="10" class="tweet-box"><c:if test="${message.id == defid }" var="flg1" /><c:if test="${flg1}" >${defcomment}</c:if></textarea>
            <input type = "hidden" name="commentUserId"  value="${loginUser.id}"  />
            <input type = "hidden" name="messageId"  value="${message.id}"  />

            <br />

            <input type="submit" value="投稿" class="postbot">
        </form>
		</div>
        <br />
			</c:forEach>

        	</div><!--messagesの-->


</div><!--センターの-->
<%-- <c:remove var="Word" scope="session"/>
<c:remove var="Start" scope="session"/>
<c:remove var="End" scope="session"/> --%>
<c:remove var="defcomment" scope="session"/>
</div><!--メインコンテンツの-->

            <div class="copyright"> Copyright(c)Sanuki.Yutaro</div>

</body>
</html>