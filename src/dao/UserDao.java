package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.SomeName;
import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_deleted");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // is_deleted
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.setInt(6, user.getIsDeleted());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String account,String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM sanuki_yutaro.users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchId = rs.getString("branch_id");
                String positionId = rs.getString("position_id");
                String isDeleted = rs.getString("is_deleted");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(Integer.parseInt(branchId));
                user.setPositionId(Integer.parseInt(positionId));
                user.setIsDeleted(Integer.parseInt(isDeleted));
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }



    private List<SomeName> toSomeName(ResultSet rs) throws SQLException {

        List<SomeName> ret = new ArrayList<SomeName>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String branchName = rs.getString("branchName");
                String positionName = rs.getString("positionName");
                int isDeleted = rs.getInt("is_deleted");

                SomeName somename = new SomeName();
                somename.setId(id);
                somename.setAccount(account);
                somename.setName(name);
                somename.setBranchName(branchName);
                somename.setPositionName(positionName);
                somename.setIs_deleted(isDeleted);

                ret.add(somename);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    public List<SomeName> getSomeName(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("sanuki_yutaro.users.id,");
            sql.append("sanuki_yutaro.users.name as name,");
            sql.append("sanuki_yutaro.users.account,");
            sql.append("sanuki_yutaro.branches.name as branchName,");
            sql.append("sanuki_yutaro.positions.name as positionName,");
            sql.append("sanuki_yutaro.users.is_deleted ");
            sql.append("FROM sanuki_yutaro.users ");
            sql.append("INNER JOIN sanuki_yutaro.branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN sanuki_yutaro.positions ");
            sql.append("ON users.position_id = positions.id;");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<SomeName> ret = toSomeName(rs);
            return ret ;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUserData(Connection connection , int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM sanuki_yutaro.users WHERE id = ? ;";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> ret = toUserList(rs);

            return ret.get(0);
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> checkuser(Connection connection , int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM sanuki_yutaro.users WHERE id = ? ;";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> ret = toUserList(rs);

            return ret ;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE ");
            sql.append("sanuki_yutaro.users ");
            sql.append("SET ");
            sql.append("account = ? , ");
            sql.append("name = ? , ");
            sql.append("branch_id = ? , ");
            sql.append("position_id = ? ");
            if(!StringUtils.isEmpty(user.getPassword())){
                sql.append("password = ? , ");
            }
            sql.append("WHERE ");
            sql.append("id = ? ;");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranchId());
            ps.setInt(4, user.getPositionId());
            if(!StringUtils.isEmpty(user.getPassword())){
                ps.setString(5, user.getPassword());
                ps.setInt(6, user.getId());
            }else{
            	ps.setInt(5, user.getId());
            }


            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void userdelete(Connection connection, int id , int flag) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE ");
            sql.append("sanuki_yutaro.users ");
            sql.append("SET ");
            sql.append("is_deleted = ?  ");
            sql.append("WHERE ");
            sql.append("id = ? ;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, flag);
            ps.setInt(2, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getAccount(Connection connection , String account) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM sanuki_yutaro.users WHERE account = ? ;";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);

            ResultSet rs = ps.executeQuery();

            List<User> ret = toUserList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



}