package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", category");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?");//subject
            sql.append(", ?");//category
            sql.append(", ?");// text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getCategory());
            ps.setString(4, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<UserMessage> getUserMessages
    (Connection connection, int num , String start , String end ,String word) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("sanuki_yutaro.messages.id as id, ");
            sql.append("sanuki_yutaro.messages.subject as subject, ");
            sql.append("sanuki_yutaro.messages.category as category, ");
            sql.append("sanuki_yutaro.messages.text as text, ");
            sql.append("sanuki_yutaro.messages.user_id as user_id, ");
            sql.append("sanuki_yutaro.users.account as account, ");
            sql.append("sanuki_yutaro.users.name as name, ");
            sql.append("sanuki_yutaro.messages.created_date as created_date ");
            sql.append("FROM sanuki_yutaro.messages ");
            sql.append("INNER JOIN sanuki_yutaro.users ");
            sql.append("ON sanuki_yutaro.messages.user_id = sanuki_yutaro.users.id ");
            sql.append("WHERE sanuki_yutaro.messages.created_date BETWEEN ? AND ? " );
            if(! StringUtils.isEmpty(word)){
            sql.append("AND sanuki_yutaro.messages.category LIKE  ?  " );
            }
            sql.append("ORDER BY created_date DESC limit " + num );
            sql.append(";");
            ps = connection.prepareStatement(sql.toString());

            Date date = new Date() ;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd- 23:59:59");
            String d = sdf.format(date);

            if(StringUtils.isEmpty(start) && StringUtils.isEmpty(end)){
            	ps.setString(1, "2018/01/01");
            	ps.setString(2, d);
            }else if(!StringUtils.isEmpty(start) && StringUtils.isEmpty(end)){
            	ps.setString(1, start);
            	ps.setString(2, d);
            }else if(StringUtils.isEmpty(start)&&!StringUtils.isEmpty(end)){
            	ps.setString(1, "2018/01/01");
            	ps.setString(2, end + " 23:59:59");
            }else if(!StringUtils.isEmpty(start) && !StringUtils.isEmpty(end)){
            	ps.setString(1, start);
            	ps.setString(2, end + " 23:59:59");
            }

            if(!StringUtils.isEmpty(word)){
        	ps.setString(3, "%" + word + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setAccount(account);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setCategory(category);
                message.setText(text);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void messagedelete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = " DELETE FROM sanuki_yutaro.messages WHERE id = ?; ";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<UserMessage> messageSearch(Connection connection, String word, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("sanuki_yutaro.messages.id as id, ");
            sql.append("sanuki_yutaro.messages.subject as subject, ");
            sql.append("sanuki_yutaro.messages.category as category, ");
            sql.append("sanuki_yutaro.messages.text as text, ");
            sql.append("sanuki_yutaro.messages.user_id as user_id, ");
            sql.append("sanuki_yutaro.users.account as account, ");
            sql.append("sanuki_yutaro.users.name as name, ");
            sql.append("sanuki_yutaro.messages.created_date as created_date ");
            sql.append("FROM sanuki_yutaro.messages ");
            sql.append("INNER JOIN sanuki_yutaro.users ");
            sql.append("ON sanuki_yutaro.messages.user_id = sanuki_yutaro.users.id ");
            sql.append(" WHERE  sanuki_yutaro.messages.category = ? ");
            sql.append("ORDER BY created_date DESC limit " + num );
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, word);

            System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


}