package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.Commented;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment){
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("text");
            sql.append(", message_id");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // text
            sql.append(", ?"); // message_id
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getMessage_id());
            ps.setInt(3, comment.getUser_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Commented> getComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("sanuki_yutaro.comments.id as id, ");
            sql.append("sanuki_yutaro.comments.message_id as message_id, ");
            sql.append("sanuki_yutaro.comments.text as text, ");
            sql.append("sanuki_yutaro.comments.user_id as user_id, ");
            sql.append("sanuki_yutaro.users.name as name, ");
            sql.append("sanuki_yutaro.comments.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN comments ");
            sql.append("ON users.id = comments.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Commented> ret = toCommentedList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Commented> toCommentedList(ResultSet rs)
            throws SQLException {

        List<Commented> ret = new ArrayList<Commented>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String user_name = rs.getString("name");
                int userId = rs.getInt("user_id");
                int messageId = rs.getInt("message_id");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Commented commented = new Commented();
                commented.setId(id);
                commented.setUser_name(user_name);
                commented.setUser_id(userId);
                commented.setMessage_id(messageId);
                commented.setText(text);
                commented.setCreatedDate(createdDate);

                ret.add(commented);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void commentdelete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = " DELETE FROM sanuki_yutaro.comments WHERE id = ?; ";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



}
