package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Commented;
import beans.User;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;




    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

         String start = request.getParameter("start");
         String end = request.getParameter("end");
         String word =request.getParameter("word");



        List<UserMessage> messages = new MessageService().getMessage(start, end, word);
        List<Commented> commented = new CommentService().getComment();

        request.setAttribute("Word", word);
        request.setAttribute("Start", start);
        request.setAttribute("End", end);
        request.setAttribute("messages", messages);
        request.setAttribute("comments", commented);
        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

}
