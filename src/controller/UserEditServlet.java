package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.EditService;
import service.IdCheckService;
import service.PositionService;

@WebServlet(urlPatterns = { "/useredit" })
public class UserEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        if (isValid(request, messages) == true) {


    	int id = Integer.parseInt(request.getParameter("id")) ;
    	User user = new EditService().edit(id);

        List<Branch> branch = new BranchService().get();
        List<Position> position = new PositionService().get();


    	  session.setAttribute("User", user);
    	  session.setAttribute("EditUser", user);
    	  session.setAttribute("Branch", branch);
    	  session.setAttribute("Position", position);

    	request.getRequestDispatcher("useredit.jsp").forward(request, response);
        }else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("manager");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response)
    throws IOException, ServletException {


        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        if (isValid(request, messages) == true) {


    	int id = Integer.parseInt(request.getParameter("id")) ;
    	User user = new EditService().edit(id);

        List<Branch> branch = new BranchService().get();
        List<Position> position = new PositionService().get();


    	  session.setAttribute("User", user);
    	  session.setAttribute("Branch", branch);
    	  session.setAttribute("Position", position);


    	 response.sendRedirect("useredit");
        }else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("useredit.jsp");
        }

        }

    private boolean isValid(HttpServletRequest request, List<String> messages) {



        if (request.getParameter("id") == null) {
            System.out.println(request.getParameter("id"));
            messages.add("不正なパラメーターです。");
        }else if (request.getParameter("id").matches( "[0-9]{1,20}") != true) {
            System.out.println(request.getParameter("id"));
            messages.add("不正なパラメーターです。");
        }else if(new IdCheckService().check(Integer.parseInt(request.getParameter("id")))){
            System.out.println(request.getParameter("id"));
        	messages.add("不正なパラメーターです。");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    }



