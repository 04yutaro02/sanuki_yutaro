package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.AccountCheckService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
    		HttpServletResponse response) throws IOException, ServletException {
    		request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
            user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

            new UserService().register(user);

            response.sendRedirect("manager");
        } else {
            session.setAttribute("errorMessages", messages);
            session.setAttribute("errorName", request.getParameter("name"));
            session.setAttribute("errorAccount", request.getParameter("account"));
            response.sendRedirect("signup");
        }
   }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String passwordcopy = request.getParameter("passwordcopy");

        if(!new AccountCheckService().check(account)){
        	messages.add("アカウント名が重複しています");
        }

        System.out.println(name.length());
        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }else if(name.length() >= 10){
        	messages.add("名前は10文字以下で入力してください");
        }


        if (StringUtils.isBlank(account) == true) {
            messages.add("アカウント名を入力してください");
        }else if (account.matches("[a-zA-Z0-9]{6,20}") != true) {
            messages.add("アカウント名は半角英数字を含む6文字以上20文字以下で入力してください");
        }
/*        if (password.matches( "[ -~]{6,20}") != true) {
            messages.add("パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
        }*/
        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }else if (password.matches( "[ -~]{6,20}") != true) {
            messages.add("パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
        }

        if (StringUtils.isBlank(passwordcopy) == true) {
            messages.add("確認用パスワードを入力してください");
        }else if (!password.equals(passwordcopy)) {
            messages.add("パスワードが一致しません");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


}