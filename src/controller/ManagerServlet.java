package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.SomeName;
import service.SomeNameService;

@WebServlet(urlPatterns = { "/manager" })
public class ManagerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    throws IOException, ServletException {


        SomeNameService SomeNameService = new SomeNameService();
        List<SomeName> somename = SomeNameService.AllList();

   request.setAttribute("somename", somename);

    	request.getRequestDispatcher("manager.jsp").forward(request, response);
    }




}
