package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.AccountCheckService;
import service.BranchService;
import service.PositionService;
import service.UpdateService;

@WebServlet(urlPatterns = { "/update" })
public class UpdateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("useredit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 List<String> messages = new ArrayList<String>();
    	 HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {
        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

        int loginId = Integer.parseInt(request.getParameter("loginId"));
        int id = Integer.parseInt(request.getParameter("id"));

        if(loginId == id ){
       	 session.setAttribute("loginUser", user);
       }

        new UpdateService().update(user);

        response.sendRedirect("manager");

    }else {
        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
        List<Branch> branch = new BranchService().get();
        List<Position> position = new PositionService().get();

        session.setAttribute("EditUser", user);
        session.setAttribute("Branch", branch);
        session.setAttribute("Position", position);
        session.setAttribute("errorMessages", messages);
        response.sendRedirect("useredit.jsp");
    }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String acc = request.getParameter("defacc");
        String password = request.getParameter("password");
        String passwordcopy = request.getParameter("passwordcopy");

        if(!acc.equals(account)){
        if(!new AccountCheckService().check(account)){
        	messages.add("アカウント名が重複しています");
        }
        }


        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isBlank(account) == true) {
            messages.add("アカウント名を入力してください");
        }
        if (account.matches( "[a-zA-Z0-9]{6,20}") != true) {
            messages.add("アカウント名は半角英数字を含む6文字以上20文字以下で入力してください");
        }
        if ( StringUtils.isBlank(password) == true && ! StringUtils.isBlank(passwordcopy) == true) {
            messages.add("パスワードを入力してください");
            if (password.matches( "[ -~]{6,20}") != true) {
                messages.add("パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
            }
        }
        if (! StringUtils.isBlank(password) == true &&  StringUtils.isBlank(passwordcopy) == true) {
            messages.add("確認用パスワードを入力してください");
            if (password.matches( "[ -~]{6,20}") != true) {
                messages.add("確認用パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
            }
        }
        if ( StringUtils.isBlank(password) != true &&  StringUtils.isBlank(passwordcopy) != true) {
            if (password.matches( "[ -~]{6,20}") != true) {
                messages.add("パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
            }
            if (password.matches( "[ -~]{6,20}") != true) {
                messages.add("確認用パスワードは半角英数字記号を含む6文字以上20文字以下で入力してください");
            }
        }
        if (!password.equals(passwordcopy)) {
            messages.add("パスワードが一致しません");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
