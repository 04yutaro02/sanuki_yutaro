package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName="LoginFilter",urlPatterns="/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
    	//System.out.println(((HttpServletRequest) request).getServletPath());

    	HttpSession session = ((HttpServletRequest) request).getSession();
    	if(!((HttpServletRequest) request).getServletPath().equals("/css/login.css")){
	    	if(((HttpServletRequest) request).getServletPath().equals("/login") ){
	    		chain.doFilter(request, response); // サーブレットを実行
	    	}else{
	    		if (session == null) {
	    			((HttpServletResponse)response).sendRedirect("./login");
	    			return ;
	    		}else if (session.getAttribute("loginUser") == null) {
	       			List<String> errorMessages = new ArrayList<String> ();
	       			errorMessages.add("ログインしていません");
	    			session.setAttribute("errorMessages", errorMessages);
	    			((HttpServletResponse)response).sendRedirect("./login");
	    			return ;
	    		}
	    		chain.doFilter(request, response); // サーブレットを実行
	    	}
    	}else{
    		chain.doFilter(request, response); // サーブレットを実行
    	}
    }

    @Override
    public void init(FilterConfig fconfig) {
    }

    @Override
    public void destroy() {
    }

}
