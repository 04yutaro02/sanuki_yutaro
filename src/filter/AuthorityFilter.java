package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="AuthorityFilter", urlPatterns={"/manager","/signup","/useredit"})
public class AuthorityFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	HttpSession session = ((HttpServletRequest) request).getSession();
    	User user = (User) session.getAttribute("loginUser");
    	int flag = user.getPositionId() ;
    	int flag2 = user.getBranchId() ;

    	if(flag == 1 && flag2 == 1){
    		chain.doFilter(request, response);
    	}else{
			List<String> errorMessages = new ArrayList<String> ();
			errorMessages.add("このユーザーは権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)response).sendRedirect("./");
			return ;
    	}

    }

    @Override
    public void init(FilterConfig fconfig) {
    }

    @Override
    public void destroy() {
    }

}
