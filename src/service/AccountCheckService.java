package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;

public class AccountCheckService {

    public boolean check(String account) {

        Connection connection = null;
        try {
            connection = getConnection();


            UserDao userDao = new UserDao();
           List<User> userlist = userDao.getAccount(connection, account);
           int Flag = userlist.size();
           if(Flag == 0 ){
        	   commit(connection);
        	   return true;
           }else{
        	   commit(connection);
        	   return false;
           }

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}