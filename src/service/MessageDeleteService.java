package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.MessageDao;

public class MessageDeleteService {

    public void delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();


            MessageDao messageDao = new MessageDao();
            messageDao.messagedelete(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}