package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.UserDao;

public class UserDeleteService {

    public void userdelete(int id , int flag) {

        Connection connection = null;
        try {
            connection = getConnection();


            UserDao userDao = new UserDao();
            if(flag == 0){
            	userDao.userdelete(connection, id,1);
            }else{
            	userDao.userdelete(connection, id,0);
            }

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}