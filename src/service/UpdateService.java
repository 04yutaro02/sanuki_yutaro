package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UpdateService {

    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isEmpty(user.getPassword())){
            String password = user.getPassword();
            String encPassword = CipherUtil.encrypt(password);
            user.setPassword(encPassword);
            }
            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}