package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;

public class EditService {
	 public User edit (int id) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            User editUser = userDao.getUserData(connection , id);

	            commit(connection);

	            return editUser;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }


}