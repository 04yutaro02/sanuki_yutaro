package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.SomeName;
import dao.UserDao;

public class SomeNameService {
	 public List<SomeName> AllList () {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            List<SomeName> ret = userDao.getSomeName(connection);

	            commit(connection);

	            return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }


}
